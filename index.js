// console.log("Hello World!");


//[SECTION] Functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

/*
	Syntax:
		function functionName() {
			code block (statement)
		}
*/

//  Function Declaration
	//function keyword - used to define a js functions
	//functionName - we set name so that we can use it for later
	//function block ({}) - this is where code to be executed

function printName(){
	console.log("My name is John");
}

printName(); //Invocation - calling a function that needs to be executed

// [Hoisting]
// behavior for certain variables and functions to run or use before their declaration
declaredFunction();
// make sure the function is existing whenever we call/invoke a function

	function declaredFunction(){
		console.log("Hello World");
	}

declaredFunction();

// Function Expression
// can be also stored in a variabl. That is called as function expression
// Hoisting is allowed in function declaration, however we could not hoist through function expression.

let variableFunction = function(){
	console.log("Hello again!");

}

variableFunction();

let functionExpression = function funcname(){
	console.log("Hello from the other side.");

}

functionExpression();

// funcName();
// Error: Whenever we are calling a named functin stores in variable, we just call the variable name, not the function name

console.log(' ');

console.log("Reassigning Functions");

declaredFunction();

declaredFunction = function(){
	console.log("updated declaredFunction");
}

declaredFunction();

functionExpression = function(){
	console.log("updated functionExpression");
}

functionExpression();

// Constant function
// 
const constantFunction = function(){
	console.log("Initialized with const");
}

constantFunction();

// Reassign constant
/*
	constantFunction = function(){
		console.log('New value');
	}	
	
	constantFunction();
*/


console.log(" ");

// functiopn with const keyword cannot be reassigned
// Scope os the accessibilty / visibilty in a code
	// 1. 

console.log("[Function Scoping]");

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

// console.log(localVar);

let globalVar = "Mr. Worldwide";
console.log(globalVar);

{
	console.log(globalVar);
}

// function with const keyword cannot be reassigned.
//Scope is the accessibility (visibility) of variables.
/*	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/

function showName(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showName();
	
	// Error -  This are function coped variable and cannot be accessed outside the function they were declared in
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

// Nested functions
	// can create another function inside a function.

function myNewFunc(){
	let name = ('Jane');

	function nestedFunc(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunc();
}

myNewFunc();

//nestedFunction(); --> will cause error because the function is located to a function scoped parent function.

//Function and Glocal Scoped Variable

//Global scpoed variable

let globalName = "Zuitt";

function myNewFunc2(){
	let nameInside = "Renz";
	console.log(globalName);
}

myNewFunc2();

// alert() allows us to show small window at the top of our browser page to show information to our user.
// Syntax: alert("Sample Alert Message!");

function sampleAlert(){
	alert("Hello, User!");
}

sampleAlert();

// alert messages inside a function will only execute whenever we call/invoke the function

console.log("I will only log in the console when the alert is dismised");

//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

//[SECTION] Using prompt()
//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

// let samplePrompt = prompt("Enter your Name.");

// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

//prompt() can be used to gather user input
//promt() it can be run immediately

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

// [SECTION] Function Naming Convention
//Function names should be definitive of the task it will perform. It usually contains a verb.

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();

//Avoid generic names to avoid confusion within your code.

function get(){
	let name = "Jamie";
	console.log(name)
}

get();

//Avoid pointless and inappropriate function names.

function foo(){ //getModolus
	console.log(25%5);
}

foo();

//Name your functions in small caps. Follow camelCase when naming variables and functions.

// camelCase ---> myNameIsRomenick
// snake_case ---> my_name_is_romenick
// kebab-case ---> my-name-is-romenick

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}

displayCarInfo();



